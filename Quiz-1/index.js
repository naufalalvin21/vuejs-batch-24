//SOAL 1
console.log("______SOAL 1______");
function jumlah_kata(kalimat) {
  kalimat = kalimat.split(" ");
  console.log(kalimat);
  var kata = 0;
  for (var i = 0; i < kalimat.length; i++) {
    if (kalimat[i] != "") {
      kata = kata + 1;
    }
  }
  return kata;
}
var kalimat_1 = "Halo nama saya Muhammad Iqbal Mubarok ";
var kalimat_2 = "Saya Iqbal";
var hitung_1 = jumlah_kata(kalimat_1);
var hitung_2 = jumlah_kata(kalimat_2);
console.log(hitung_1, hitung_2);

//SOAL 2
console.log("______SOAL 2______");
function next_date(tanggal, bulan, tahun) {
  var kabisat = 0;
  if (tahun % 4 == 0) {
    kabisat = 1;
  }
  switch (bulan) {
    case 1: {
      if (tanggal <= 31) {
        if (tanggal == 31) {
          console.log(1, "Februari", tahun);
        } else {
          console.log(tanggal + 1, "Januari", tahun);
        }
      } else {
        console.log("Output Error");
      }
      break;
    }
    case 2: {
      if (kabisat == 1) {
        if (tanggal <= 29) {
          if (tanggal == 29) {
            console.log(1, "Maret", tahun);
          } else {
            console.log(tanggal + 1, "Februari", tahun);
          }
        } else {
          console.log("Output Error");
        }
      } else if (kabisat == 0) {
        if (tanggal <= 28) {
          if (tanggal == 28) {
            console.log(1, "Maret", tahun);
          } else {
            console.log(tanggal + 1, "Februari", tahun);
          }
        } else {
          console.log("Output Error");
        }
      }
      break;
    }
    case 3: {
      if (tanggal <= 31) {
        if (tanggal == 31) {
          console.log(1, "April", tahun);
        } else {
          console.log(tanggal + 1, "Maret", tahun);
        }
      } else {
        console.log("Output Error");
      }
      break;
    }
    case 4: {
      if (tanggal <= 30) {
        if (tanggal == 30) {
          console.log(1, "Mei", tahun);
        } else {
          console.log(tanggal + 1, "April", tahun);
        }
      } else {
        console.log("Output Error");
      }
      break;
    }
    case 5: {
      if (tanggal <= 31) {
        if (tanggal == 31) {
          console.log(1, "Juni", tahun);
        } else {
          console.log(tanggal + 1, "Mei", tahun);
        }
      } else {
        console.log("Output Error");
      }
      break;
    }
    case 6: {
      if (tanggal <= 30) {
        if (tanggal == 30) {
          console.log(1, "Juli", tahun);
        } else {
          console.log(tanggal + 1, "Juni", tahun);
        }
      } else {
        console.log("Output Error");
      }
      break;
    }
    case 7: {
      if (tanggal <= 31) {
        if (tanggal == 31) {
          console.log(1, "Agustus", tahun);
        } else {
          console.log(tanggal + 1, "Juli", tahun);
        }
      } else {
        console.log("Output Error");
      }
      break;
    }
    case 8: {
      if (tanggal <= 31) {
        if (tanggal == 31) {
          console.log(1, "September", tahun);
        } else {
          console.log(tanggal + 1, "Agustus", tahun);
        }
      } else {
        console.log("Output Error");
      }
      break;
    }
    case 9: {
      if (tanggal <= 30) {
        if (tanggal == 30) {
          console.log(1, "Oktober", tahun);
        } else {
          console.log(tanggal + 1, "September", tahun);
        }
      } else {
        console.log("Output Error");
      }
      break;
    }
    case 10: {
      if (tanggal <= 31) {
        if (tanggal == 31) {
          console.log(1, "November", tahun);
        } else {
          console.log(tanggal + 1, "Oktober", tahun);
        }
      } else {
        console.log("Output Error");
      }
      break;
    }
    case 11: {
      if (tanggal <= 30) {
        if (tanggal == 30) {
          console.log(1, "Desember", tahun);
        } else {
          console.log(tanggal + 1, "November", tahun);
        }
      } else {
        console.log("Output Error");
      }
      break;
    }
    case 12: {
      if (tanggal <= 31) {
        if (tanggal == 31) {
          console.log(1, "Januari", tahun + 1);
        } else {
          console.log(tanggal + 1, "Desember", tahun);
        }
      } else {
        console.log("Output Error");
      }
      break;
    }
  }
}

var tanggal = 29;
var bulan = 2;
var tahun = 2020;
next_date(tanggal, bulan, tahun);

var tanggal = 28;
var bulan = 2;
var tahun = 2021;

next_date(tanggal, bulan, tahun);

var tanggal = 31;
var bulan = 12;
var tahun = 2020;

next_date(tanggal, bulan, tahun);
