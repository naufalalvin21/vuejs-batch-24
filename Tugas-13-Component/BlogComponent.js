export const BlogComponent = {
  template: `
    <div>    
      <h1>{{blog.title}}</h1>
      <p>{{content}}</p>
    </div>
  `,
  data() {
    return {
      pesan: "ini dari component blog",
    };
  },
  props: ["title", "content"],
};
