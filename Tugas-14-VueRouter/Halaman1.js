export const Halaman1 = {
  data() {
    return {
      alat: [
        {
          id: 1,
          title: "Waw",
        },
        {
          id: 2,
          title: "Wiw",
        },
        {
          id: 3,
          title: "Wuw",
        },
      ],
    };
  },
  template: `
      <div>
          <h1>Daftar Item</h1>
          <ul>
              <li v-for="item of alat">
                  <p>{{ item.title }}</p>
              </li>
          </ul>
      </div>
  `,
};
