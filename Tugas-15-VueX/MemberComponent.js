export const MemberComponents = {
  props: {
    members: {
      type: Array,
      default: () => [],
    },
  },
  template: `
  <div >
    <tr v-for="member of members">
      <th>
        <img
          src="http://3.bp.blogspot.com/-Qajc7p9PC84/T2npQtFLhEI/AAAAAAAAAEM/dPMJ2YzuQgY/s1600/alvin.jpg"
          alt=""
        />
      </th>
      <th>
        <p>Name : {{member.name}}</p>
        <p>Address : {{member.address}}</p>
        <p>No_Hp : {{member.no_hp}}</p>
      </th>
      <th>
        <button @click="$emit('editMember', member)">Edit</button>
        <button @click="removeMember(member.id)">Hapus</button>
        <button @click="uploadPhoto(member)">Upload Foto</button>
      </th>
    </tr>
  </div>`,
};
