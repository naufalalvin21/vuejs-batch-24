// SOAL 1
var pertama = "saya sangat senang hari ini";
var kedua = "belajar javascript itu keren";
keduaUpper = kedua.toUpperCase();
console.log("________Soal 1________");
console.log(pertama.substring(0, 4), pertama.substring(12, 18), kedua.substring(0, 7), keduaUpper.substring(8, 19));

// SOAL 2
var kataPertama = "10";
var kataKedua = "2";
var kataKetiga = "4";
var kataKeempat = "6";
var IntkataPertama = parseInt(kataPertama);
var IntkataKedua = parseInt(kataKedua);
var IntkataKetiga = parseInt(kataKetiga);
var IntkataKeempat = parseInt(kataKeempat);
console.log("________Soal 2________");
console.log(
  "(",
  kataKedua,
  " x ",
  kataKetiga,
  ")",
  " + ",
  kataPertama,
  "+",
  kataKeempat,
  " = ",
  IntkataKedua * IntkataKetiga + IntkataPertama + IntkataKeempat
);

// SOAL 3
var kalimat = "wah javascript itu keren sekali";

var kataPertama = kalimat.substring(0, 3);
var kataKedua = kalimat.substring(4, 14); // do your own!
var kataKetiga = kalimat.substring(15, 18); // do your own!
var kataKeempat = kalimat.substring(19, 24); // do your own!
var kataKelima = kalimat.substring(25, 31); // do your own!
console.log("________Soal 3________");
console.log("Kata Pertama: " + kataPertama);
console.log("Kata Kedua: " + kataKedua);
console.log("Kata Ketiga: " + kataKetiga);
console.log("Kata Keempat: " + kataKeempat);
console.log("Kata Kelima: " + kataKelima);
