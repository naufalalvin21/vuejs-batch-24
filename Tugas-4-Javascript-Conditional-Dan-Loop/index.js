//SOAL 1
var nilai = 78;
console.log("______SOAL 1______");
if (nilai >= 85) {
  console.log("Indeks Nilai = A");
} else if (nilai >= 75 && nilai < 85) {
  console.log("Indeks Nilai = B");
} else if (nilai >= 65 && nilai < 75) {
  console.log("Indeks Nilai = C");
} else if (nilai >= 55 && nilai < 65) {
  console.log("Indeks Nilai = D");
} else {
  console.log("Indeks Nilai = E");
}

//SOAL 2
var tanggal = 30;
var bulan = 5;
var tahun = 2000;
console.log("______SOAL 2______");
switch (bulan) {
  case 1: {
    console.log(tanggal, "Januari", tahun);
    break;
  }
  case 2: {
    console.log(tanggal, "Februari", tahun);
    break;
  }
  case 3: {
    console.log(tanggal, "Maret", tahun);
    break;
  }
  case 4: {
    console.log(tanggal, "April", tahun);
    break;
  }
  case 5: {
    console.log(tanggal, "Mei", tahun);
    break;
  }
  case 6: {
    console.log(tanggal, "Juni", tahun);
    break;
  }
  case 7: {
    console.log(tanggal, "Juli", tahun);
    break;
  }
  case 8: {
    console.log(tanggal, "Agustus", tahun);
    break;
  }
  case 9: {
    console.log(tanggal, "September", tahun);
    break;
  }
  case 10: {
    console.log(tanggal, "Oktober", tahun);
    break;
  }
  case 11: {
    console.log(tanggal, "November", tahun);
    break;
  }
  case 12: {
    console.log(tanggal, "Desember", tahun);
    break;
  }
}

//SOAL 3
var n = 5;
console.log("______SOAL 3______");
for (var i = 1; i <= n; i++) {
  var hash = "";
  for (var j = 1; j <= i; j++) {
    hash += "#";
  }
  console.log(hash);
}

//SOAL 4
console.log("______SOAL 4______");
var m = 7;
var i = 1;
temp = 0;
while (i <= m) {
  if (i % 3 == 0 && i > 0) {
    console.log(i, "I love Vuejs");
    console.log("===");
    temp = 0;
  } else {
    temp = temp + 1;
    if (temp == 1) {
      console.log(i, "I love Programming");
    } else if (temp == 2) {
      console.log(i, "I love Javascript");
    }
  }
  i = i + 1;
}

// for (var p = 1; p <= m; p = p + 2) {
//   console.log(p, " - I love programming");
//   for (var q = 2; q <= p; q = q + 2) {
//     console.log(q, " - I love Javascript");
//     for (var r = 3; r <= q; r = r + 2) {
//       console.log(r, " - I love Vuejs");
//     }
//   }

// if (p % 2 == 1) {
//   if (p % 3 == 0) {
//     console.log(p, " - I Love VueJS");
//   } else {
//     console.log(p, " - I Love programming");
//   }
// } else {
//   console.log(p, " - I love Javascript");
// }
//}
