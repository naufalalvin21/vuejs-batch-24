//SOAL 1
console.log("_____SOAL 1_____");
var daftarHewan = ["2. Komodo", "5. Buaya", "3. Cicak", "4. Ular", "1. Tokek"];
for (var i = 0; i < daftarHewan.length; i++) {
  console.log(daftarHewan.sort()[i]);
}

//SOAL 2
console.log("_____SOAL 2_____");
function introduce(data) {
  return (
    "Nama saya " +
    data.name +
    ", umur saya " +
    data.age +
    " tahun, alamat saya di " +
    data.address +
    ", dan saya punya hobby yaitu " +
    data.hobby
  );
}
var data = { name: "John", age: 30, address: "Jalan Pelesiran", hobby: "Gaming" };
var perkenalan = introduce(data);
console.log(perkenalan); // Menampilkan "Nama saya John, umur saya 30 tahun, alamat saya di Jalan Pelesiran, dan saya punya hobby yaitu Gaming"

//SOAL 3
console.log("_____SOAL 3_____");

function hitung_huruf_vokal(nama) {
  nama = nama.toUpperCase();
  nama = nama.split("");
  var vokal = 0;
  for (var i = 0; i <= nama.length; i++) {
    if (nama[i] == "I" || nama[i] == "A" || nama[i] == "U" || nama[i] == "E" || nama[i] == "O") {
      vokal = vokal + 1;
    }
  }
  return vokal;
}
var hitung_1 = hitung_huruf_vokal("Muhammad");

var hitung_2 = hitung_huruf_vokal("Iqbal");

console.log(hitung_1, hitung_2); // 3 2

//SOAL 4
console.log("_____SOAL 4_____");
function hitung(nilai) {
  if (nilai == 0) {
    return -2;
  } else {
    var angka = -2;
    for (var i = 1; i <= nilai; i++) {
      angka = angka + 2;
    }
    return angka;
  }
}
console.log(hitung(0)); // -2
console.log(hitung(1)); // 0
console.log(hitung(2)); // 2
console.log(hitung(3)); // 4
console.log(hitung(5)); // 8
