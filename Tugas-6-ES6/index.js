//SOAL 1
console.log("______SOAL 1______");
const luas = (p, l) => p * l;
const keliling = (p, l) => 2 * p + 2 * l;
L = luas(4, 5);
K = keliling(4, 5);
console.log(L, K);

//SOAL 2
console.log("______SOAL 2______");
const newFunction = (firstName, lastName) => {
  return {
    firstName,
    lastName,
    fullName: function () {
      console.log(`${firstName} ${lastName}`);
    },
  };
};

//Driver Code
newFunction("William", "Imoh").fullName();

//SOAL 3
console.log("______SOAL 3______");
const newObject = {
  firstName: "Muhammad",
  lastName: "Iqbal Mubarok",
  address: "Jalan Ranamanyar",
  hobby: "playing football",
};
const { firstName, lastName, address, hobby } = newObject;
console.log(firstName, lastName, address, hobby);

//SOAL 4
console.log("______SOAL 4______");
const west = ["Will", "Chris", "Sam", "Holly"];
const east = ["Gill", "Brian", "Noel", "Maggie"];
const combined = west.concat(east);
//Driver Code
console.log(combined);

let combinedArr = [...west, ...east];
console.log(combinedArr);

//SOAL 5
console.log("______SOAL 5______");
const planet = "earth";
const view = "glass";
var before = `Lorem ${view} dolor sit amet, consectetur adipiscing elit, ${planet}`;
console.log(before);
