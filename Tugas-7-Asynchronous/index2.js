var readBooksPromise = require("./promise.js");

var books = [
  { name: "LOTR", timeSpent: 3000 },
  { name: "Fidas", timeSpent: 2000 },
  { name: "Kalkulus", timeSpent: 4000 },
];
// Lanjutkan code untuk menjalankan function readBooksPromise
let time = 10000;
i = 0;

const bacaBuku = (time) => {
  if (time <= 0 || i >= books.length) {
    return 0;
  } else {
    readBooksPromise(time, books[i]).then((timeLeft) => {
      i += 1;
      return bacaBuku(timeLeft);
    });
  }
};

bacaBuku(time);
